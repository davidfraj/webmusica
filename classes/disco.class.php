<?php  

class Disco{

	public $nombre;
	public $año;
	public $grupo;
	public $canciones;
	public $portada;

	public function __construct($nombre, $año, $grupo, $canciones, $portada){

		$this->nombre=$nombre;
		$this->año=$año;
		$this->grupo=$grupo;
		$this->canciones=$canciones;
		$this->portada=$portada;
		
	}

}
?>